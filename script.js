// JSCRIPT 200 Class 3 Homework 4

// Class Exercise 1 
// Food is Cooked 
const foodIsCooked = function(kind, internalTemp, doneness) {
    if (kind === 'chicken' && internalTemp >= 165) {
      return true;
    } else if (kind === 'beef' && doneness === 'rare' && internalTemp >= 125) {
        return true;
    } else if (kind === 'beef' && doneness === 'medium' && internalTemp >= 135) {
    return true;
    } else if (kind === 'beef' && doneness === 'well' && internalTemp >= 155) {
      return true;
    } else {
      return false;
    };
};

// console.log(foodIsCooked('beef', 124, 'rare'));
// console.log(foodIsCooked('beef', 125, 'rare'));
// console.log(foodIsCooked('beef', 125, 'medium'));
// console.log(foodIsCooked('beef', 135, 'medium'));
// console.log(foodIsCooked('beef', 144, 'well'));
// console.log(foodIsCooked('beef', 156, 'well'));

// Class Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true
/////////////////////////////////////

// Class Exercise 2 
// Create a Deck of Cards

/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */

 // Create new Card constructor
const Card = function Card (i, suitString) {
  this.val = i;
  this.displayVal = i;
  this.suit = suitString;
  switch (i) {
    case (11):
      this.displayVal = 'Jack';
      this.val = 10;
      break;
    case (12):
      this.displayVal = 'Queen';
      this.val = 10;
      break;
    case (13):
      this.displayVal = 'King';
      this.val = 10;
      break;
    case (14):
      this.displayVal = 'Ace';
      this.val = 1;
      break;
  }; // end switch statement
};
//const testCard = new Card(11, 'Spades');
//console.log (testCard);


// Create new Deck here
const getDeck = function () {
  const deck = [];
  const suits = ['hearts', 'spades', 'clubs', 'diamonds'];
  for (let suit in suits) {
    for (let i = 2; i < 15; i++) {
      // Construct new card
      const card = new Card(i, suits[suit]);
      deck.push(card); // add card to end of deck

    };  // end suit loop
  }; // end suits loop
  return deck;  // return deck
}; // end deck function

console.log(getDeck());

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && 
    randomCard.displayVal && 
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);

/////////////////////////////////////




// Class 4 Homework 1
// Blackjack.js

const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
const CardPlayer = function(name) {
  // CREATE CardPlayer Object HERE
  this.name = name;
  this.hand = [];
  this.drawCard = function() { // Draw random card from deck
    let randomCardIndex = Math.floor(Math.random()*getDeck().length); // Select index of random card in new deck array
    // console.log(randomCardIndex);
    this.hand.push(getDeck()[randomCardIndex]);  // Add card from new deck to hand
  };
};

// CREATE TWO NEW CardPlayers
let dealer = new CardPlayer('Dealer')
let player = new CardPlayer('Player');

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  // CREATE calcPoint object HERE, with total and isSoft
  this.total = 0;
  this.isSoft = true;
  for (let card in hand) {  // loop through cards in hand
    if (hand[card].val === 1 && this.total < 11) { // Check if card is an Ace and less than 11 points in hand
      this.total += 11; // Add 11 true
      this.isSoft = false; 
    } else {
      this.total += hand[card].val;  // Add value of each card to total
    };
  };

  //console.log(`     total is ${this.total}`);
  // console.log(`     isSoft is ${this.isSoft}`);
  //console.log(`object is ${this}`);

  return this;
};

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) { // Accepts array of card objects
  // return boolean 
  // check if total is 16 or less --OR-- if dealer has 11 point Ace and 17 points
  if ((calcPoints(dealerHand).total < 17) || (!calcPoints(dealerHand).isSoft && calcPoints(dealerHand).total === 17)) {
    //console.log(`dealerShouldDraw returns true`);
    return true;
  } else {
    //console.log(`dealerShouldDraw returns false`);
    return false;
  };
};

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) { 
  // returns a string of scores  and who wins HERE
  let winner;
  playerScore > dealerScore ? winner = 'Player wins': dealerScore > playerScore ? winner = 'Dealer wins' : winner = 'Tie' ;
  let msg =  `Player has ${playerScore} points. Dealer has ${dealerScore} points. ${winner}.`
  return msg ;
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();
  // console.log(player);
  for (let i in player.hand){
    console.log( player.hand[i]);
  };
  //showHand(player)

  // EXTRA CREDIT PART I)
  //var table = displayArrayAsTable(player.hand, 0, player.hand.length-1);
  //document.body.appendChild(table);
  //var element = document.getElementById("playerHand");
  //element.appendChild(player.hand);  

  console.log((player.hand[0].val), player.hand[1].val);
  console.log(`Player hand total score: ${(calcPoints(player.hand)).total}`);  // doesn't work. returns undefined
  showHand(player);
  let playerScore = calcPoints(player.hand).total;
  if (playerScore === 21) {  // if player immediately draws 2 cards .... EXTRA CREDIT PART J)
    // skip to determineWinner
    console.log(`Player wins with 21!`);
  } else {  // else draw cards and go to dealer's turn
    while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
      player.drawCard();
      showHand(player);
      playerScore = calcPoints(player.hand).total;
      console.log(`Player has ${playerScore}.`)
    };
    if (playerScore > 21) {
      return 'You went over 21 - you lose!';
    };
    console.log(`Player stands at ${playerScore}`);

    // IMPLEMENT DEALER LOGIC BELOW
    showHand(dealer);
    let dealerScore = calcPoints(dealer.hand).total;
    if (dealerScore === 21) {
      // skip to determineWinner.... EXTRA CREDIT PART J)
      console.log(`Dealer has Blackjack...`)
    } else {
      while (dealerShouldDraw(dealer.hand) && dealerScore < 21) { 
        dealer.drawCard();
        showHand(dealer);
        dealerScore = calcPoints(dealer.hand).total;
        console.log(`Dealer has ${dealerScore}.`)
      };
      if (dealerScore > 21) {
        return 'Dealer busts.';
      };
      console.log(`Dealer stands at ${dealerScore}`);
    };

    console.log(determineWinner(playerScore, dealerScore));
  };
};
console.log(startGame());


// Extra Section.. IN PROGRESS
// function displayArrayAsTable(array, from, to ) {
//   var array = array || [],
//       from = from || 0,
//       to = to || 0;
//   if ( array.length < 1 )
//   {
//     return;
//   }

//   if ( to == 0 )
//   {
//       to = array.length - 1;
//   }

//   var table = document.createElement('table');
//   var row = document.createElement('row');

//   for ( var x = from; x < to + 1; x++ ) 
//   {
//     var td = document.createElement('td');
//       td.innerHTML = array[x];
//     row.appendChild(td);
//   }

//   table.appendChild(row);  
//   return table;
// }

// var table = displayArrayAsTable(QR4, 24, 25);
// document.body.appendChild(table);